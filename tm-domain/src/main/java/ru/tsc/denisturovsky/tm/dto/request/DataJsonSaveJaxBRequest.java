package ru.tsc.denisturovsky.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonSaveJaxBRequest extends AbstractUserRequest {

    public DataJsonSaveJaxBRequest(@Nullable String token) {
        super(token);
    }

}
