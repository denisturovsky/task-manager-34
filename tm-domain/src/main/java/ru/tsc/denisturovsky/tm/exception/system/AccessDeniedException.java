package ru.tsc.denisturovsky.tm.exception.system;

import ru.tsc.denisturovsky.tm.exception.field.AbstractFieldException;

public final class AccessDeniedException extends AbstractFieldException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}
