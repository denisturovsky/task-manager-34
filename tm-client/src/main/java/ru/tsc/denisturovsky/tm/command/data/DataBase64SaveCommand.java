package ru.tsc.denisturovsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in base64 file.";

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        getDomainEndpointClient().base64SaveData(request);
    }

}
