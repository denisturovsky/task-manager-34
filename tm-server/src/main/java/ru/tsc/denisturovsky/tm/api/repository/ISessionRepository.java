package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
