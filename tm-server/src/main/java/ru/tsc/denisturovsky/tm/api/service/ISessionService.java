package ru.tsc.denisturovsky.tm.api.service;

import ru.tsc.denisturovsky.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
