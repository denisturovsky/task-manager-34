package ru.tsc.denisturovsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M remove(
            @Nullable String userId,
            @Nullable M model
    );

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    M add(
            @Nullable String userId,
            @Nullable M model
    );

    void clear(@Nullable String userId);

    int getSize(@Nullable String userId);

}
